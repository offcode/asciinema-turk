import json
import re
import subprocess

def _exec(line):
        return subprocess.run(line, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, shell=True)

def exec_line(line):
    """
    >>> exec_line('# Just a comment')
    ("# Just a comment", '', '')
    >>> exec_line('echo something')
    ("echo something", "something", "")
    >>> exec_line('echo nothing > /dev/null')
    ("echo nothing > /dev/null", '', '')
    >>> exec_line('rmdir nonexistent')
    ("rmdir nonexistent", "", "rmdir: failed to remove 'nonexistent': No such file or directory")
    """
    try:
        cp = _exec(line)
        return (line, cp.stdout, cp.stderr)
    except Exception as ce:
        #import pdb; pdb.set_trace()
        return (line, ce.stdout, ce.stderr)

def no_exec(line):
    if re.search('^\s*export ', line):
        return True
    if re.search('&\s*$', line):
        return True
    if re.search('asciinema: no-exec', line):
        return True

def exec_script(fname):
    with open(fname) as script:
        for line in script.readlines():
            if no_exec(line):
                yield([line, None, None])
            else:
                yield exec_line(line)

def nice_line(timer, raw_line, newline=True):
    if isinstance(raw_line, str):
        line = raw_line
    else:
        line = raw_line.decode('utf-8')
    if newline:
        line = line.rstrip() + "\r\n"
    return json.dumps([timer, "o", line])
        
def make_cast(script_file, prompt):
    yield json.dumps({"version": 2, "width": 94, "height": 51, "timestamp": 1552049462, "env": {"SHELL": "/bin/bash", "TERM": "xterm"}})
    timer = 0
    for line_result in exec_script(script_file):
        cmd, stdout, stderr = line_result
        yield nice_line(timer, prompt, newline=False)
        yield nice_line(timer, cmd)
        result = stdout or stderr
        if result:
            yield nice_line(timer, result)
        timer += 1

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser('Gimme a script')
    parser.add_argument('-e', dest='command')
    parser.add_argument('-s', dest='script')
    parser.add_argument('-c', dest='cast', action='store_true')
    args = parser.parse_args()
    if args.command:
        print(exec_line(args.command))
    elif args.script:
        if args.cast:
            for line in make_cast(args.script, '> '):
                print(line)
        else:
            result = exec_script(args.script)
            print(result)